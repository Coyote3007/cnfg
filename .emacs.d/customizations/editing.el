
;; Key binding to use "hippie expand" for text autocompletion
(global-set-key (kbd "C-c C-c SPC") 'hippie-expand)

;; Highlights matching parenthesis
(show-paren-mode 1)

;; autocomplete
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; In every buffer, the line which contains the cursor will be fully
;; highlighted
(global-hl-line-mode 1)

;; Parenthesis mode
(show-paren-mode 1)
