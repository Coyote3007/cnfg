(define-package "serverspec" "20150119.250" "Serverspec minor mode"
  '((dash "2.6.0")
    (s "1.9.0")
    (f "0.16.2")
    (helm "1.6.1"))
  :url "http://101000lab.org")
;; Local Variables:
;; no-byte-compile: t
;; End:
