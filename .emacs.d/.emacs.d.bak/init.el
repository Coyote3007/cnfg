;; Turn off mouse interface early in startup to avoid momentary display
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; Ample-zen
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'ample-zen t)

(require 'tramp)
(setq tramp-default-method "ssh")

;; No splash screen please ... jeez
(setq inhibit-startup-message t)

;; Melpa FTW
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")))

;; Write backup files to own directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name
                 (concat user-emacs-directory "backups")))))

;; Disallow accidental exits
confirm-kill-emacs 'y-or-n-p  

;; Haskel-mode YAY
(add-hook 'haskell-mode-hook 'haskell-indentation-mode)
(setq auto-mode-alist
	(append '(("\\.[he]s$" . haskell-mode) 
		  ("\\.l[he]s$" . haskell-mode)  ; for literate prgs
		  ("\\.hi$" . haskell-mode))    ; for interface files
		  auto-mode-alist))

(autoload 'haskell-mode "haskell-mode"
     "Major mode for editing Haskell scripts." t)
(autoload 'literate-haskell-mode "haskell-mode"
     "Major mode for editing literate Haskell scripts." t)

(setq haskell-use-font-lock t)          ; enable font-lock highlighting
(setq haskell-use-decl-scan t)          ; enable imenu


  ; use haskell-doc mode for pop-up type info on fcts etc
(autoload 'turn-on-haskell-doc-mode "haskell-doc" nil t)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)


frame-title-format '(buffer-file-name "%f" "%b") ; I know it's Emacs; show something useful


;; Language specific setup files
(eval-after-load 'js2-mode '(require 'setup-js2-mode))
(eval-after-load 'ruby-mode '(require 'setup-ruby-mode))
(eval-after-load 'clojure-mode '(require 'setup-clojure-mode))
(eval-after-load 'markdown-mode '(require 'setup-markdown-mode))

;; Load stuff on demand
(autoload 'skewer-start "setup-skewer" nil t)
(autoload 'skewer-demo "setup-skewer" nil t)
(autoload 'auto-complete-mode "auto-complete" nil t)
(eval-after-load 'flycheck '(require 'setup-flycheck))

;; Don't use expand-region fast keys
(setq expand-region-fast-keys-enabled nil)

;; Show expand-region command used
(setq er--show-expansion-message t)


;; Run at full power please
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)


